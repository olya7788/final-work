import { useState, useEffect } from "react";
import "./App.css";
import React from 'react';
import { App } from './App';
import ProductList from "./Components/ProductList/ProductList";
import FetchProducts from "./Components/FetchProducts/FetchProducts";
import { Route } from "react-router-dom";
import { Routes } from "react-router-dom";
import { Registration } from "./Pages/Registration";
import { Login } from "./Pages/Login";
import Product from "./Components/Product/Product";
import { useSelector } from "react-redux";
import ReactDOM from 'react-dom';
import  Basket from "./Pages/Basket";
import { Provider } from 'react-redux';
import createStore from './createReduxStore';

const store = createStore()
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <Provider store={store}>
    <App />
  </Provider>
)


function  App () {
  const isAuth = useSelector((state) => state.auth.isAuth);
  const productsFromRedux = useSelector((state) => state.products.products);

  useEffect(() => {
    setProducts(productsFromRedux);
  }, [productsFromRedux]);

  const [products, setProducts] = useState(productsFromRedux);

  
  const [searchValue, setSearchValue] = useState("");


  const [foundedProducts, setFoundedProducts] = useState([]);

 
  useEffect(() => {
    setFoundedProducts(products);
  }, [products]);

 
  const titles = [];
  
  if (products.length > 0) {
    for (let i = 0; i < products.length; i++) {
      titles.push({
        title: products[i].title.toLowerCase(),
        id: products[i].id,
      });
    }
  }

 
  if (searchValue.length >= 3) {
    titles.forEach((item) => {
      if (item.title.includes(searchValue.toLowerCase())) {
        foundSearchIds.push(item.id);
      }
    });
  }

 
  let tempFoundProducts = [];
  products.forEach((item) => {
    if (foundSearchIds.includes(item.id)) {
      tempFoundProducts.push(item);
    }
  });

  useSelector(() => {
    if (searchValue.length >= 3) {
      setFoundedProducts([...tempFoundProducts]);
    } else {
      setFoundedProducts(products);
    }
  }, [searchValue ]);

  const categories = ["все товары"];

  if (products.length > 0) {
    for (let i = 0; i < products.length; i++) {
      if (!categories.includes(products[i].category)) {
        categories.push(products[i].category);
      }
    }
  }

  const [selectValue, setSelectValue] = useState("все товары");

  let tempFoundProductsFromSelect = [];
  products.forEach((item) => {
    if (item.category === selectValue) {
      tempFoundProductsFromSelect.push(item);
    }
  });

  useSelector(() => {
    if (selectValue !== "все товары") {
      setFoundedProducts([...tempFoundProductsFromSelect]);
    } else {
      setFoundedProducts(products);
    }
  }, [selectValue]);

  return (
    <div className="App">
      {isAuth && <FetchProducts />}

      <hr />

      <Routes>
        <Route
          path="/product/:id"
          element={
            isAuth ? <Product allProducts={products} /> : <Registration />
          }
        ></Route>

        <Route
          path="/basket"
          element={isAuth ? <Basket /> : <Registration />}
        ></Route>

        <Route
          path="/products"
          element={
            isAuth ? (
              <ProductList
                products={foundedProducts}
                searchValue={searchValue}
                setSearchValue={setSearchValue}
                categories={categories}
                selectValue={selectValue}
                setSelectValue={setSelectValue}
                allProducts={products}
              />
            ) : (
              <Registration />
            )
          }
        ></Route>

        <Route path="/login" element={<Login />}></Route>

        <Route path="/" element={<Registration />}></Route>
      </Routes>
    </div>
  );
}

export default App;
   